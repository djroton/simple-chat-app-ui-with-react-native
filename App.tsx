import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { SWRConfig } from 'swr';

export default function App() {
 const fetcher = url => fetch(url).then(r => r.json())
 const [message, setMessage] = useState("");
 const [messages, setMessages] = useState([]);

 const handleMessageSubmit = () => {
   if (message.trim()) {
     setMessages([...messages, message]);
     setMessage("");
   }
 };

 return (
   <SafeAreaProvider>
   <SWRConfig
    value={{
      fetcher,
    }}
  >
   <View style={styles.container}>
     <View style={styles.messagesContainer}>
       {messages.map((msg, index) => (
         <View key={index} style={styles.message}>
           <Text style={styles.messageText}>{msg}</Text>
         </View>
       ))}
     </View>

     <View style={styles.inputContainer}>
       <TextInput
         style={styles.textInput}
         value={message}
         onChangeText={(text) => setMessage(text)}
         placeholder="Send a message..."
       />
       <TouchableOpacity style={styles.sendButton} onPress={handleMessageSubmit}>
         <Text style={styles.sendButtonText}>Send</Text>
       </TouchableOpacity>
     </View>
   </View>
   </SWRConfig>
   </SafeAreaProvider>
 );
};

const styles = StyleSheet.create({
 container: {
   flex: 1,
   backgroundColor: "#fff",
 },
 messagesContainer: {
   flex: 1,
   padding: 16,
 },
 message: {
   backgroundColor: "#e6e6e6",
   paddingHorizontal: 8,
   paddingVertical: 4,
   borderRadius: 5,
   marginBottom: 8,
   alignSelf: "flex-start",
   maxWidth: "80%",
 },
 messageText: {
   fontSize: 16,
   color: "#000",
 },
 inputContainer: {
   flexDirection: "row",
   alignItems: "center",
   padding: 16,
 },
 textInput: {
   flex: 1,
   height: 40,
   backgroundColor: "#e6e6e6",
   borderRadius: 20,
   paddingHorizontal: 16,
   marginRight: 16,
 },
 sendButton: {
   backgroundColor: "#007aff",
   borderRadius: 20,
   paddingVertical: 8,
   paddingHorizontal: 16,
 },
 sendButtonText: {
   color: "#fff",
   fontSize: 16,
 },
});